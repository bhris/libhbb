#!/bin/bash
#
# This script can be used to automatically rebuild hbb if any source file
# changed. To use this script inotifywait (under Debian from inotify-tools)
# must be installed.

echo "Starting autobuild..."

while inotifywait -q -q -e close_write \
        ./src \
        ./test \
        ./Language/Haskell/HBB \
        ./Language/Haskell/HBB/TestModes \
        ./Language/Haskell/HBB/Internal; do
    clear
    cabal build 2>&1 | head -n 55
done
