module LibSrcSpanSpec where

import Language.Haskell.HBB.Internal.SrcSpan
import Test.Hspec
import FastString (mkFastString)
import SrcLoc

spec :: Spec
spec = do
    describe "splitAtBufLoc" $ do
        it "is doesn't fail on empty input lists" $
            (splitAtBufLoc [] (BufLoc 3 1)) `shouldBe` ([],[])
        it "succeeds in splitting a single line" $
            (splitAtBufLoc 
                ["fact x = if x <= 1 then 1 else x * fact (x-1)"] 
                (BufLoc 1 1)) 
            `shouldBe`
            ([""],["fact x = if x <= 1 then 1 else x * fact (x-1)"])
        it "succeeds in splitting a multiple lines" $
            (splitAtBufLoc 
                ["fact 1 = 1"
                ,"fact x = x * fact (x-1)"]
                (BufLoc 2 14)) 
            `shouldBe`
            (["fact 1 = 1"
             ,"fact x = x * "],
             ["fact (x-1)"])
        it "should succeed in a specific case (problem from 2014-06-21)" $
            splitAtBufLoc 
                ["module Main (main) where"
                ,""
                ,"data Lang = English | German"
                ,""
                ,"transHello English = \"Hello\""
                ,"transHello German  = \"Hallo\""
                ,""
                ,"hello :: String -> Int -> String"
                ,"hello l 1 = l ++ \" first world!\""
                ,"hello l 2 = l ++ \" second world!\""
                ,"hello l 3 = let s = \" third world!\" "
                ,            "in l ++ s"
                ,"hello l x | x /= 10 && l /= \"\" = l ++ (show x) ++ \"th world!\""
                ,          "| otherwise          = l ++ (show x) ++ \"th world, you are the winner!\""]
                (BufLoc 9 33)
            `shouldBe`
                (["module Main (main) where"
                 ,""
                 ,"data Lang = English | German"
                 ,""
                 ,"transHello English = \"Hello\""
                 ,"transHello German  = \"Hallo\""
                 ,""
                 ,"hello :: String -> Int -> String"
                 ,"hello l 1 = l ++ \" first world!\""],
                 [""
                 ,"hello l 2 = l ++ \" second world!\""
                 ,"hello l 3 = let s = \" third world!\" "
                 ,            "in l ++ s"
                 ,"hello l x | x /= 10 && l /= \"\" = l ++ (show x) ++ \"th world!\""
                 ,          "| otherwise          = l ++ (show x) ++ \"th world, you are the winner!\""])
    describe "disjoint" $ do
        it "returns true when two RealSrcSpans do not overlap" $
            disjoint
                (mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 1 10) 
                    (mkRealSrcLoc (mkFastString "") 1 12))
                (mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 1 13) 
                    (mkRealSrcLoc (mkFastString "") 1 16))
        it "returns true when two RealSrcSpans do not overlap (2)" $
            disjoint
                (mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 1 10) 
                    (mkRealSrcLoc (mkFastString "") 1 12))
                (mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 1 12) 
                    (mkRealSrcLoc (mkFastString "") 1 14))
    describe "leq" $ do
        it "returns true if SrcSpans are really ordered" $
            leq 
                (mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 1 10) 
                    (mkRealSrcLoc (mkFastString "") 1 12))
                (mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 1 13) 
                    (mkRealSrcLoc (mkFastString "") 1 16))
            `shouldBe` True
        it "returns true if SrcSpans are really ordered (2)" $
            leq 
                (mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 1 10) 
                    (mkRealSrcLoc (mkFastString "") 2 12))
                (mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 2 12) 
                    (mkRealSrcLoc (mkFastString "") 3 20))
            `shouldBe` True
        it "returns False if the first SrcSpan is at a later position" $
            leq 
                (mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 3 12) 
                    (mkRealSrcLoc (mkFastString "") 4 20))
                (mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 1 10) 
                    (mkRealSrcLoc (mkFastString "") 2 12))
            `shouldBe` False
    describe "splitBufferedLinesAtBufSpan" $ do
        it "Should split a single line correctly" $
            splitBufferedLinesAtBufSpan
                ["fact x = if x <= 1 then 1 else x * fact (x-1)"]
                (toBufSpan $ mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 1 36) 
                    (mkRealSrcLoc (mkFastString "") 1 40))
            `shouldBe` (["fact x = if x <= 1 then 1 else x * "],["fact"],[" (x-1)"])
        it "Should split multiple lines correctly" $
            splitBufferedLinesAtBufSpan
                ["fact 1 = 1"
                ,"fact x = x * fact (x-1)"]
                (toBufSpan $ mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 1 8) 
                    (mkRealSrcLoc (mkFastString "") 2 8))
            `shouldBe` (["fact 1 "],["= 1","fact x "],["= x * fact (x-1)"])
        it "Should split multiple lines correctly" $
            splitBufferedLinesAtBufSpan
                ["fact 0 = 1"
                ,"fact 1 = 1"
                ,"fact x = x * fact (x-1)"]
                (toBufSpan $ mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 2 1) 
                    (mkRealSrcLoc (mkFastString "") 2 5))
            `shouldBe` (["fact 0 = 1",""],["fact"],[" 1 = 1","fact x = x * fact (x-1)"])
        it "Should work with the problem-example from 2014-06-21" $
            splitBufferedLinesAtBufSpan
                ["module Main (main) where"
                ,""
                ,"data Lang = English | German"
                ,""
                ,"transHello English = \"Hello\""
                ,"transHello German  = \"Hallo\""
                ,""
                ,"hello :: String -> Int -> String"
                ,"hello l 1 = l ++ \" first world!\""
                ,"hello l 2 = l ++ \" second world!\""
                ,"hello l 3 = let s = \" third world!\" "
                ,            "in l ++ s"
                ,"hello l x | x /= 10 && l /= \"\" = l ++ (show x) ++ \"th world!\""
                ,          "| otherwise          = l ++ (show x) ++ \"th world, you are the winner!\""]
                (toBufSpan $ mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "examples/PlayHelloPattern.hs") 9 13) 
                    (mkRealSrcLoc (mkFastString "examples/PlayHelloPattern.hs") 9 33))
            `shouldBe`
                (["module Main (main) where"
                 ,""
                 ,"data Lang = English | German"
                 ,""
                 ,"transHello English = \"Hello\""
                 ,"transHello German  = \"Hallo\""
                 ,""
                 ,"hello :: String -> Int -> String"
                 ,"hello l 1 = "],
                 ["l ++ \" first world!\""],
                 [""
                 ,"hello l 2 = l ++ \" second world!\""
                 ,"hello l 3 = let s = \" third world!\" "
                 ,            "in l ++ s"
                 ,"hello l x | x /= 10 && l /= \"\" = l ++ (show x) ++ \"th world!\""
                 ,          "| otherwise          = l ++ (show x) ++ \"th world, you are the winner!\""])
        it "Should split a point-buf-span correctly" $
            splitBufferedLinesAtBufSpan
                ["|  -> "]
                (toBufSpan $ mkRealSrcSpan 
                    (mkRealSrcLoc (mkFastString "") 1 3) 
                    (mkRealSrcLoc (mkFastString "") 1 3))
            `shouldBe` (["| "],[""],[" -> "])
    describe "joinSplit" $ do
        it "Should be able to assemble a single-line split" $
            joinSplit
                (["hello "],["world!"])
            `shouldBe` ["hello world!"]
        it "Should be able to assemble a one-sided split (1)" $
            joinSplit
                (["hello world!"],[])
            `shouldBe` ["hello world!"]
        it "Should be able to assemble a one-sided split (2)" $
            joinSplit
                ([],["hello world!"])
            `shouldBe` ["hello world!"]
        it "Should be able to assemble a multiline-split" $
            joinSplit
                (["fact 1 = 1","fact x = x * "],["fact (x-1)"])
            `shouldBe` ["fact 1 = 1","fact x = x * fact (x-1)"]
        it "Should be able to assemble a multiline-split with one empty side" $
            joinSplit
                (["fact 1 = 1","fact x = x * fact (x-1)"],[])
            `shouldBe` ["fact 1 = 1","fact x = x * fact (x-1)"]
    describe "str2LineBuf" $ do
        it "Should create a line buffer of a single line string" $
            str2LineBuf
                "let y = x*x"
            `shouldBe`
                ["let y = x*x"]
        it "Should create a line buffer with two lines" $
            str2LineBuf
                "let y = x*x\nin  y"
            `shouldBe`
                ["let y = x*x"
                ,"in  y"]
        it "Should be able to handle trailing newlines correctly (in opposite to 'lines')" $
            str2LineBuf
                "|  ->\n"
            `shouldBe`
                ["|  ->"
                ,""]
        it "Should be able to handle leading newlines correctly" $
            str2LineBuf
                "\n  \n  "
            `shouldBe`
                [""
                ,"  "
                ,"  "]
        it "Should be able to handle multiple leading newlines correctly" $
            str2LineBuf
                "\n\n  \n  "
            `shouldBe`
                [""
                ,""
                ,"  "
                ,"  "]
