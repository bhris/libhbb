module LibTTreeSpec where

import Language.Haskell.HBB.Internal.SrcSpan
import Language.Haskell.HBB.Internal.TTree
import Test.Hspec
import FastString (mkFastString)
import SrcLoc

spec :: Spec
spec = do
    describe "applyTTree" $ do
        it "it should be able to apply a tree with a single addition" $
            applyTTree
                []
                ( 
                    (BufSpan
                        (BufLoc 2 14)
                        (BufLoc 2 18)),
                    (TTree
                        (Addition ["()"])
                        []))
                ["fact 1 = 1"
                ,"fact x = x * fact (x-1)"]
            `shouldBe`
                ["fact 1 = 1"
                ,"fact x = x * () (x-1)"]
        it "it should be able to apply a tree with two Additions (one child)" $
            let n = (
                        (BufSpan
                            (BufLoc 1 2)
                            (BufLoc 1 2)),
                        (TTree
                            (Addition ["\\x -> if x <= 1 then 1 else x * fact (x-1)"])
                            []))
            in applyTTree
                []
                (
                    (BufSpan
                        (BufLoc 2 14)
                        (BufLoc 2 18)),
                    (TTree
                        (Addition ["()"])
                        [n]))
                ["fact 1 = 1"
                ,"fact x = x * fact (x-1)"]
            `shouldBe`
                ["fact 1 = 1"
                ,"fact x = x * (\\x -> if x <= 1 then 1 else x * fact (x-1)) (x-1)"]
        it "it should be able to handle a special case discovered at 2014-07-10" $
            -- This example lead to a bug in str2LineBuf which was fixed
            -- 2014-07-11...
            let tree = TTree
                    (Addition [""
                              ,"                      "
                              ,"                      "
                              ,""])
                    [
                        (BufSpan 
                            (BufLoc 3 23)
                            (BufLoc 3 23)
                        ,TTree
                            (Display ((mkRealSrcSpan
                                (mkRealSrcLoc (mkFastString "examples/PlayHelloPattern.hs") 16  1)
                                (mkRealSrcLoc (mkFastString "examples/PlayHelloPattern.hs") 16 25)),0))
                            []
                        ),
                        (BufSpan 
                            (BufLoc 2 23)
                            (BufLoc 2 23)
                        ,TTree
                            (Display ((mkRealSrcSpan
                                (mkRealSrcLoc (mkFastString "examples/PlayHelloPattern.hs") 15  1)
                                (mkRealSrcLoc (mkFastString "examples/PlayHelloPattern.hs") 15 26)),0))
                            []
                        ),
                        (BufSpan 
                            (BufLoc 1 1)
                            (BufLoc 1 1)
                        ,TTree
                            (Display ((mkRealSrcSpan
                                (mkRealSrcLoc (mkFastString "examples/PlayHelloPattern.hs") 14 15)
                                (mkRealSrcLoc (mkFastString "examples/PlayHelloPattern.hs") 14 26)),0))
                            []
                        )
                    ]
            in  applyTTree
                [
                    (
                        "examples/PlayHelloPattern.hs",
                        ["module Main (main) where"
                        ,""
                        ,"import Data.Char (isSpace)"
                        ,""
                        ,"data Lang = English | German"
                        ,""
                        ,"main :: IO ()"
                        ,"main = print $ hello (transHello English) (quad (single 2)) 1"
                        ,""
                        ,"transHello English = \"Hello\""
                        ,"transHello German  = \"Hallo\""
                        ,""
                        ,"hello :: String -> Int -> Int -> String"
                        ,"hello l 1 1 | l /= \"\" && "
                        ,"              l /= \"_\" &&"
                        ,"              isNotSpace = l ++ "
                        ,"                           \" first world!\""
                        ,"              where "
                        ,"                  isNotSpace :: Bool"
                        ,"                  isNotSpace = not $ "
                        ,"                               any isSpace l"
                        ,"hello l 2 1 = let s = \" second world!\" "
                        ,"              in l ++ s"
                        ,"hello l 3 1 = l ++ \" third world!\""
                        ,"hello l x y | x /= 10 && l /= \"\" = l ++ (show x) ++ \"th world!\""
                        ,"            | otherwise          = "
                        ,"                  l ++ (show x) ++ \"th world, you are the winner!\""
                        ,""]
                    )
                ]
                (
                    (BufSpan
                        (BufLoc 1 1)
                        (BufLoc 1 1))
                    ,tree
                )
                [""]
            `shouldBe`
                ["l /= \"\" && "
                ,"                                    l /= \"_\" &&"
                ,"                                    isNotSpace"
                ,""]
        it "it should be able to inline a display" $
            applyTTree
                [("example.hs",["fact 1 = 1"
                               ,"fact x = x * fact (x-1)"])]
                (
                    (BufSpan
                        (BufLoc 2 14)
                        (BufLoc 2 18)),
                    (TTree
                        (Addition ["(\\x -> case x of 1 -> "
                                  ,"                              x -> )"])
                        [
                            (
                                (BufSpan (BufLoc 1 23) (BufLoc 1 23)),
                                (TTree
                                    (Display ((mkRealSrcSpan (mkRealSrcLoc (mkFastString "example.hs") 1 10)
                                                             (mkRealSrcLoc (mkFastString "example.hs") 1 11)),0))
                                    [])
                            ),
                            (
                                (BufSpan (BufLoc 2 36) (BufLoc 2 36)),
                                (TTree
                                    (Display ((mkRealSrcSpan (mkRealSrcLoc (mkFastString "example.hs") 2 10)
                                                             (mkRealSrcLoc (mkFastString "example.hs") 2 24)),0))
                                    [])
                            )
                        ]
                    )
                )
                ["fact 1 = 1"
                ,"fact x = x * fact (x-1)"]
            `shouldBe`
                ["fact 1 = 1"
                ,"fact x = x * (\\x -> case x of 1 -> 1"
                ,"                              x -> x * fact (x-1)) (x-1)"]
