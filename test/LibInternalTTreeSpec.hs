module LibInternalTTreeSpec where

import Language.Haskell.HBB.Internal.InternalTTree
import Language.Haskell.HBB.Internal.SrcSpan
import Language.Haskell.HBB.Internal.TTree
import Test.Hspec
import FastString (mkFastString)
import SrcLoc

spec :: Spec
spec = do
    describe "applyInsertionInfo" $ do
        it "it should be able to apply a single addition" $
            applyInsertionInfo
                0 0 [] [] [] 0 0 0 []
                (((IncInline $ pointBufSpan 1 1),16),TTree (Addition ["\\x -> case x of "]) [])
            `shouldBe`
            [((pointBufSpan 1 1,16),TTree (Addition ["\\x -> case x of "]) [])]
        it "it should be able to apply a second addition to the previous result" $
            applyInsertionInfo
                0 0 [] [] [] 0 0 0
                [((pointBufSpan 1 1,16),TTree (Addition ["\\x -> case x of "]) [])]
                ((IncInline $ pointBufSpan 1 2,1),TTree (Addition ["1"]) [])
            `shouldBe`
            [((pointBufSpan 1 2, 1),TTree (Addition ["1"]                ) [])
            ,((pointBufSpan 1 1,16),TTree (Addition ["\\x -> case x of "]) [])]
        it "it should be able to apply a nested addition" $
            applyInsertionInfo
                0 0 [] [] [] 0 0 0 []
                (
                    (IncInline (pointBufSpan 1 1),17),
                    TTree 
                        (Addition ["\\x -> case x of "]) 
                        [(
                            (IncInline (pointBufSpan 1 17),1),
                            TTree 
                                (Addition ["x"]) 
                                []
                        )]
                )
            `shouldBe`
            [(
                    (pointBufSpan 1 1,17),
                    TTree 
                        (Addition ["\\x -> case x of "]) 
                        [(
                            pointBufSpan 1 17,
                            TTree
                                (Addition ["x"])
                                []
                        )]
            )]
        it "it should be able to inline a multiline nested addition" $
            applyInsertionInfo
                0 0 [] [] [] 0 0 0 []
                (
                    (IncInline $ pointBufSpan 1 1,19),
                    TTree 
                        (Addition ["\\x -> case x of  "]) 
                        [(
                            (IncInline $ pointBufSpan 1 17,2),
                            TTree
                                (Addition ["(1"
                                          ,",2)"])
                                []
                        )]
                )
            `shouldBe`
            [(
                (pointBufSpan 1 1,19),
                TTree 
                    (Addition ["\\x -> case x of  "]) 
                    [(
                        pointBufSpan 1 17,
                        TTree
                            (Addition ["(1"
                                      ,"                ,2)"])
                            []
                    )]
            )]
        it "it should be able to apply a NewSection addition correctly" $
            -- Following picture:
            --
            -- \x -> case x of 1 ->
            --
            applyInsertionInfo
                0 0 [] [] [] 0 0 0 []
                (
                    (IncInline $ pointBufSpan 1 1,16),
                    TTree 
                        (Addition ["\\x -> case x of "]) 
                        [(
                            (NewSection 1,4),
                            TTree 
                                (Addition ["1 ->"])
                                []
                        )]
                )
            `shouldBe`
            [(
                (pointBufSpan 1 1,16),
                TTree
                    (Addition ["\\x -> case x of "]) 
                    [(
                        pointBufSpan 1 17,
                        TTree
                            (Addition ["1 ->"])
                            []
                    )]
            )]
        it "it should be able to apply a nested display" $
            applyInsertionInfo
                0 0 [] [] [] 0 0 0 []
                (
                    (IncInline $ pointBufSpan 1 1,17),
                    TTree 
                        (Addition ["\\x -> case x of "]) 
                        [(
                            (IncInline $ pointBufSpan 1 17,1),
                            TTree 
                                -- Display for one charachter (e.g. "1")
                                (Display  (mkRealSrcSpan
                                        (mkRealSrcLoc (mkFastString "") 1 5)
                                        (mkRealSrcLoc (mkFastString "") 1 6)))
                                []
                        )]
                )
            `shouldBe`
            [(
                (pointBufSpan 1 1,17),
                TTree
                    (Addition ["\\x -> case x of "]) 
                    [(
                        pointBufSpan 1 17,
                        TTree
                            (Display (mkRealSrcSpan
                                    (mkRealSrcLoc (mkFastString "") 1 5)
                                    (mkRealSrcLoc (mkFastString "") 1 6),12))
                            []
                    )]
            )]
        it "it should be able to apply a two nested displays as new sections" $
            -- 
            -- \x -> case x of 1
            --                 2
            --
            applyInsertionInfo
                0 0 [] [] [] 0 0 0 []
                (
                    (IncInline $ pointBufSpan 1 1,16),
                    TTree 
                        (Addition ["\\x -> case x of "]) 
                        [(
                            (NewSection 1,1),
                            TTree 
                                -- Display for one charachter (e.g. "1")
                                (Display  (mkRealSrcSpan
                                        (mkRealSrcLoc (mkFastString "") 10 5)
                                        (mkRealSrcLoc (mkFastString "") 10 6)))
                                []
                        ),(
                            (NewSection 2,1),
                            TTree 
                                -- Display for one charachter (e.g. "2")
                                (Display  (mkRealSrcSpan
                                        (mkRealSrcLoc (mkFastString "") 2 5)
                                        (mkRealSrcLoc (mkFastString "") 2 6)))
                                []
                        )]
                )
            `shouldBe`
            [(
                (pointBufSpan 1 1,16),
                TTree
                    (Addition ["\\x -> case x of ",
                               "                "]) 
                    [(
                        pointBufSpan 2 17,
                        TTree
                            (Display (mkRealSrcSpan
                                    (mkRealSrcLoc (mkFastString "") 2 5)
                                    (mkRealSrcLoc (mkFastString "") 2 6),12))
                            []
                    ),(
                        pointBufSpan 1 17,
                        TTree
                            (Display (mkRealSrcSpan
                                    (mkRealSrcLoc (mkFastString "") 10 5)
                                    (mkRealSrcLoc (mkFastString "") 10 6),12))
                            []
                    )]
            )]
        it "it should be able to apply a special case discovered at 2014-08-05" $
            -- TTree to generate following function (part):
            --
            -- fact l 1 1 | l /= "" && 
            --              l /= "_" =
            --
            applyInsertionInfo
                0 0 [] [] [] 0 0 0 []
                (
                    (IncInline $ pointBufSpan 1 1,11),
                    TTree 
                        (Addition ["  "]) 
                        [(
                            (IncInline $ pointBufSpan 1 1,4),
                            TTree 
                                (Addition ["fact"])
                                []
                        ),(
                            (IncInline $ pointBufSpan 1 2,5), -- "l 1 1"
                            TTree 
                                (Display  (mkRealSrcSpan
                                        (mkRealSrcLoc (mkFastString "") 16  6)
                                        (mkRealSrcLoc (mkFastString "") 16 11)))
                                []
                        ),(
                            (NewSection 1,12),
                            TTree 
                                (Addition ["|  ="])
                                [(
                                    (IncInline $ pointBufSpan 1 3,8),
                                    TTree
                                        (Display (mkRealSrcSpan
                                            (mkRealSrcLoc (mkFastString "") 17 14)
                                            (mkRealSrcLoc (mkFastString "") 18 22)))
                                        []
                                )]
                        )]
                )
            `shouldBe`
            [(
                (pointBufSpan 1 1,11),
                TTree
                    (Addition ["  "]) 
                    [(
                        pointBufSpan 1 3,
                        TTree
                            (Addition ["|  ="])
                            [(
                                pointBufSpan 1 3,
                                TTree
                                    (Display (mkRealSrcSpan
                                            (mkRealSrcLoc (mkFastString "") 17 14)
                                            (mkRealSrcLoc (mkFastString "") 18 22),0))
                                    []
                            )]
                    ),(
                        pointBufSpan 1 2,
                            TTree 
                                (Display  (mkRealSrcSpan
                                        (mkRealSrcLoc (mkFastString "") 16  6)
                                        (mkRealSrcLoc (mkFastString "") 16 11),0))
                                []
                    ),(
                        pointBufSpan 1 1,
                        TTree (Addition ["fact"]) []
                    )]
            )]
        it "it should be able to surround a function by parenthesis" $
            -- TTree to generate following function (part):
            --
            -- (fact 1 =
            --  fact x =)
            --
            applyInsertionInfo
                0 0 [] [] [] 0 0 0 []
                (
                    (IncInline $ pointBufSpan 1 1,2),
                    TTree 
                        (Addition ["()"]) 
                        [(
                            (IncInline $ pointBufSpan 1 2,0),
                            TTree 
                                (Addition [""])
                                [(
                                    (NewSection 1,8),
                                    TTree 
                                        (Addition ["fact 1 ="])
                                        []
                                ),(
                                    (NewSection 2,8),
                                    TTree 
                                        (Addition ["fact x ="])
                                        []
                                )]
                        )]
                )
            `shouldBe`
            [(
                (pointBufSpan 1 1,2),
                TTree
                    (Addition ["()"]) 
                    [(
                        pointBufSpan 1 2,
                        TTree
                            (Addition [""
                                      ," "])
                            [(
                                pointBufSpan 2 2,
                                TTree
                                    (Addition ["fact x ="])
                                    []
                            ),(
                                pointBufSpan 1 2,
                                TTree
                                    (Addition ["fact 1 ="])
                                    []
                            )]
                    )]
            )]
        it "it should be able to apply a special case discovered at 2014-08-06" $
            -- TTree to generate following function (part):
            --
            -- other | Begin of source code
            -- apply somefunction =               (first  line is IncInline)
            --       somefunction =               (second line is NewSection)
            --       |
            --
            -- Aim: It should be possible to pass a global (static) offset to
            --      applyInsertionInfo by using IncInline and an according
            --      indentation value.
            applyInsertionInfo
                0 0 [] [] [] 0 0 0 []
                (
                    (IncInline $ pointBufSpan 1 7,0),
                    TTree
                        (Addition [""])
                        [(
                            (NewSection 1,14),
                            TTree 
                                (Addition [" ="]) 
                                [(
                                    (IncInline $ pointBufSpan 1 1,12),
                                    TTree 
                                        (Addition ["somefunction"])
                                        []
                                )]
                        ),(
                            (NewSection 2,14),
                            TTree
                                (Addition [" ="])
                                [(
                                    (IncInline $ pointBufSpan 1 1,12),
                                    TTree 
                                        (Addition ["somefunction"])
                                        []
                                )]
                        )]
                )
            `shouldBe`
            [(
                (pointBufSpan 1 7,0),
                TTree
                    (Addition [""
                              ,"      "]) 
                    [(
                        pointBufSpan 2 7,
                        TTree
                            (Addition [" ="])
                            [(
                                (pointBufSpan 1 1),
                                TTree
                                    (Addition ["somefunction"])
                                    []
                            )]
                    ),(
                        pointBufSpan 1 7,
                        TTree
                            (Addition [" ="])
                            [(
                                pointBufSpan 1 1,
                                TTree
                                    (Addition ["somefunction"])
                                    []
                            )]
                    )]
            )]
        it "it should be able to apply a special case discovered at 2014-08-06 (next one)" $
            -- TTree to generate following function (part):
            --
            -- somefunction x | ...
            --                where                    <-- New Section
            --                    <display element>    <-- New (Sub-)Section
            --

            applyInsertionInfo
                0 0 [] [] [] 0 0 0 []
                (
                    (IncInline $ pointBufSpan 1 1,15),
                    TTree
                        (Addition ["somefunction x "])
                        [(
                            (NewSection 1,5),
                            TTree 
                                (Addition ["| ..."]) 
                                []
                        ),(
                            (NewSection 2,9),
                            TTree
                                (Addition ["where"
                                          ,"    "])
                                [(
                                    (NewSection 1,5),
                                    TTree 
                                        (Display (mkRealSrcSpan
                                            (mkRealSrcLoc (mkFastString "") 1  6)
                                            (mkRealSrcLoc (mkFastString "") 1 11)))
                                        []
                                )]
                        )]
                )
            `shouldBe`
            [(
                (pointBufSpan 1 1,15),
                TTree
                    (Addition ["somefunction x "
                              ,"               "]) 
                    [(
                        pointBufSpan 2 16,
                        TTree
                            (Addition ["where"
                                      ,"                   "])
                            [(
                                pointBufSpan 2 20,
                                TTree
                                    (Display ((mkRealSrcSpan
                                        (mkRealSrcLoc (mkFastString "") 1  6)
                                        (mkRealSrcLoc (mkFastString "") 1 11)),14))
                                    []
                            )]
                    ),
                    (
                        pointBufSpan 1 16,
                        TTree
                            (Addition ["| ..."])
                            []
                    )]
            )]
        it "it should be able to apply a special case discovered at 2014-08-05 (extended example)" $
            -- TTree to generate following function (part):
            --
            -- fact l 1 1 | l /= "" && 
            --              l /= "_" =
            --            | otherwise =
            applyInsertionInfo
                0 0 [] [] [] 0 0 0 []
                (
                    (IncInline $ pointBufSpan 1 1,11),
                    TTree 
                        (Addition ["  "]) 
                        [(
                            (IncInline $ pointBufSpan 1 1,4),
                            TTree 
                                (Addition ["fact"])
                                []
                        ),(
                            (IncInline $ pointBufSpan 1 2,5),
                            TTree 
                                (Display  (mkRealSrcSpan
                                        (mkRealSrcLoc (mkFastString "") 1  6)
                                        (mkRealSrcLoc (mkFastString "") 1 11)))
                                []
                        ),(
                            (NewSection 1,11),
                            TTree 
                                (Addition ["|  ="])
                                [(
                                    (IncInline $ pointBufSpan 1 3,8),
                                    TTree
                                        (Display (mkRealSrcSpan
                                            (mkRealSrcLoc (mkFastString "") 1 14)
                                            (mkRealSrcLoc (mkFastString "") 2 22)))
                                        []
                                )]
                        ),(
                            (NewSection 2,13),
                            TTree
                                (Addition ["|  ="])
                                [(
                                    (IncInline $ pointBufSpan 1 3,9),
                                    TTree
                                        (Display (mkRealSrcSpan
                                            (mkRealSrcLoc (mkFastString "") 3 14)
                                            (mkRealSrcLoc (mkFastString "") 3 23)))
                                        []
                                )]
                        )]
                )
            `shouldBe`
            [(
                (pointBufSpan 1 1,11),
                TTree
                    (Addition ["  "
                              ,"           "]) 
                    [(
                        pointBufSpan 2 12,
                        TTree
                            (Addition ["|  ="])
                            [(
                                pointBufSpan 1 3,
                                TTree
                                    (Display (mkRealSrcSpan
                                            (mkRealSrcLoc (mkFastString "") 3 14)
                                            (mkRealSrcLoc (mkFastString "") 3 23),0))
                                    []
                            )]
                    ),(
                        pointBufSpan 1 3,
                        TTree
                            (Addition ["|  ="])
                            [(
                                pointBufSpan 1 3,
                                TTree
                                    (Display (mkRealSrcSpan
                                            (mkRealSrcLoc (mkFastString "") 1 14)
                                            (mkRealSrcLoc (mkFastString "") 2 22),0))
                                    []
                            )]
                    ),(
                        pointBufSpan 1 2,
                        TTree 
                            (Display  (mkRealSrcSpan
                                    (mkRealSrcLoc (mkFastString "") 1  6)
                                    (mkRealSrcLoc (mkFastString "") 1 11),0))
                            []
                    ),(
                        pointBufSpan 1 1,
                        TTree (Addition ["fact"]) []
                    )]
            )]
