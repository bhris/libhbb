{-# OPTIONS -Wall #-}

module Language.Haskell.HBB.Internal.InterfaceTypes (
    NonFirstLinesIndenting(..)
    ) where


-- | Instances of this data type control whether the non-first lines of a
-- binding in mode /inline/ or /smart-inline/ should get some extra indentation
-- to match the environment of the place where the binding should be written
-- to.
--
-- Assumed that the following call to /factorial/ should get inlined:
--
-- @
-- result = factorial 3
-- @
--
-- The function /factorial/ looks this way:
--
-- @
-- factorial 1 = 1
-- factorial x = x * factorial (x-1)
-- @
--
-- The lambda function that is produced in mode /inline/ with /IgnoreIndOfTargetEnv/
-- looks as follows:
--
-- @
-- (\x -> case x of 1 -> 1
--                  y -> y * factorial (y - 1))
-- @
--
-- The lambda function produced in mode /inline/ with /AdaptIndToTargetEnv/
-- looks as follows:
--
-- @
-- (\x -> case x of 1 -> 1
--                           y -> y * factorial (y - 1))
-- @
data NonFirstLinesIndenting = AdaptIndToTargetEnv -- ^ Increase indentation of non-first lines
                                                  --   by the column number the inlined version of the
                                                  --   the function starts at (short: adapt indentation
                                                  --   to caller environment)
                            | IgnoreIndOfTargetEnv -- ^ Preserve indentation which means don't adapt
                                                   --   the indentation to the environment of the caller.
