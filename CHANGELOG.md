
Version 0.4.1.0
---------------
 - Adding support for inlining value bindings with guards or *where*
   expressions. If such a value bindings is encountered, a trivial *case*
   expression is added `case () of () | ..."`.
 - Fixing a bug that prevented functions with one match and a single parameter
   to be inlined correctly. In this case a prefix like `\a case a of` has been
   produced, which is invalid as `->` is missing.

Version 0.4.0.2
---------------

 - Fixing a bug in the modes *inline* and *smart-inline* which caused them to
   produce a wrong representation for small functions that have no leading case
   expression. The parameters of these function were always aggregated to
   tuples.
 - Adding CHANGELOG.md and README.md to cabal package.

Version 0.4.0.1
---------------

 - The modes *locate*, *inline* and *smart-inline* are now able to
   inline/locate functions that are defined in infix notation.

Version 0.4.0.0
----------------

 - Added the integration mode *apply-ttree*.
 - The modes *inline* and *smart-inline* are now able converting a *where*
   expression to a *let* expression if this makes it possible to omit a leading
   *case* expression. The result is a much shorter lambda function.
