# **hbb**: Extraordinary Haskell programming

This project aims to create a tool which should be easily embeddable in text
editors to assist them to provide extraordinary editing features for the
Haskell programming language. To archieve this, the tool is based on the
library of the Glasgow Haskell Compiler (GHC).

The name **hbb** is short for *h*askell *b*usy *b*ee and should remind one of
the programmers using it. It consists of the library *hbb* and a command line
tool which has the name *libhbb-cli*. *libhbb-cli* has been chosen because in
another repository the features of (the library) *hbb* and *ghc-mod* are merged
into an executable *hbb* which provides (many more) features than *libhbb-cli*.

One outstanding feature of *hbb* is is the possibility to inline a function
binding.
